import pandas as pd

def loader(files, usecols, field):
    dfs = list()
    for i in files:
        try:
            df_temp = pd.read_csv(i, sep = ';', usecols = usecols, encoding = 'cp1251')
        except:
            print('Не удалось обработать файл {}, необходимо проверить его формат и содержимое'.format(i))
        else:
            dfs.append(df_temp)
    df = pd.concat(dfs, ignore_index = True)
    if 'TransactionID' in df.columns:
        df.rename(columns = {field: 'tPaymentID'}, inplace = True)
    else:
        df.rename(columns = {field: 'PaymentID'}, inplace = True)
        df.dropna(subset = ['PaymentID'], inplace = True)
        df.drop_duplicates(subset = ['PaymentID'], inplace = True)
    return df

def cols(key):
    columns = {
        'trans_cols': [
            'PaymentID',
            'TransactionID',
            'Дата_создания',
            'Дата_проведения',
            'Статус',
            'ID_поставщика',
            'ID_клиента',
            'Имя_клиента',
            'ID_мерчанта',
            'Код_ответа',
            'ID_канала',
            'Тип_транзакции',
            'Тип_операции',
            'Сумма_платежа',
            'ID_сервиса_поставщика',
            'Размер_комиссии',
            'Номер_карты',
            '3Dsecure',
            'Email_клиента',
            'Телефон_клиента',
            'RRN',
            'доп_поле_1',
            'доп_поле_2',
            'доп_поле_3',
            'доп_поле_4',
            'доп_поле_5',
            'доп_поле_6',
            'доп_поле_7',
            'доп_поле_8',
            'order_id',
            'Тип_инструмента',
        ],
        'apps_cols': [
            'ID транзакции мандарин',
            'Номер договора',
            'Дата и время заявки',
            'Сумма',
            'КБК офиса выдачи',
            'Организация выдачи',
        ],
        'pays_cols': [
            'ID транзакции мандарин',
            'Номер договора',
            'Дата получения денежных средств на счет, руб.',
            'Сумма платежа',
            'КБК офиса выдавшего займ',
            'Организация',
        ],
        'serv_cols': [
            'ID транзакции',
            'Номер договора',
            'Дата погашения',
            'Общая сумма платежа',
            'КБК выдачи займа',
            'Организация',
        ],
    }
    return columns[key]
