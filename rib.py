import warnings
warnings.filterwarnings('ignore')
import pandas as pd
from glob import glob
import os
from supp.func import loader, cols

base_path = os.getcwd()
f_trans = glob(os.path.join(base_path, 'reps', '*transaction*.csv'))
f_apps = glob(os.path.join(base_path, 'reps', '*applications*.csv'))
f_pays = glob(os.path.join(base_path, 'reps', '*payments*.csv'))
f_serv = glob(os.path.join(base_path, 'reps', '*servicepays*.csv'))
assert len(f_trans) != 0 and len(f_apps) != 0 and len(f_pays) != 0 and len(f_serv) != 0

df_trans = loader(f_trans, cols('trans_cols'), 'PaymentID')
df_apps = loader(f_apps, cols('apps_cols'), 'ID транзакции мандарин')
df_pays = loader(f_pays, cols('pays_cols'), 'ID транзакции мандарин')
df_serv = loader(f_serv, cols('serv_cols'), 'ID транзакции')

df_trans['Дата_создания'] = list(map(lambda x: pd.to_datetime(x, dayfirst = True).date(), df_trans['Дата_создания']))
df_trans['Дата_проведения'] = list(map(lambda x: pd.to_datetime(x, dayfirst = True).date(), df_trans['Дата_проведения']))
df_apps['Дата и время заявки'] = list(map(lambda x: pd.to_datetime(x, yearfirst = True).date(), df_apps['Дата и время заявки']))
df_pays['Дата получения денежных средств на счет, руб.'] = list(map(lambda x: pd.to_datetime(x, dayfirst = True).date(),
                                                                    df_pays['Дата получения денежных средств на счет, руб.']))
df_serv['Дата погашения'] = list(map(lambda x: pd.to_datetime(x, yearfirst = True).date(), df_serv['Дата погашения']))

df_ribcoz_iss = pd.merge(left = df_apps[['PaymentID', 'Номер договора', 'Дата и время заявки', 'Сумма',
                                         'КБК офиса выдачи', 'Организация выдачи']],
                         right = df_trans, left_on = 'PaymentID', right_on = 'tPaymentID', how = 'outer')

df_ribcoz_iss.rename(columns = {'Номер договора': 'Договор', 'Дата и время заявки': 'Дата из ПО',
                                'Сумма': 'Сумма из ПО'}, inplace = True)

df_ribcoz_iss = df_ribcoz_iss[(pd.isnull(df_ribcoz_iss['PaymentID']) == False)\
                            & (pd.isnull(df_ribcoz_iss['tPaymentID']) == False)]

assert len(df_ribcoz_iss['Организация выдачи'].unique()) == 1
df_ribcoz_iss.drop(columns = ['Организация выдачи', 'tPaymentID'], inplace = True)

df_ribcoz_iss['Сумма из ПО'] = list(map(lambda x: float(str(x).replace(',', '.')), df_ribcoz_iss['Сумма из ПО']))
df_ribcoz_iss['Сумма_платежа'] = list(map(lambda x: float(str(x).replace(',', '.')), df_ribcoz_iss['Сумма_платежа']))

df_ribcoz_iss['delta'] = df_ribcoz_iss['Сумма из ПО'] - df_ribcoz_iss['Сумма_платежа']
if set(df_ribcoz_iss['delta']) == {0}:
    res = ['пройден', 'не сформирован']
else:
    res = ['не пройден', 'сформирован']
    df_delta = df_ribcoz_iss[df_ribcoz_iss['delta'] != 0]
    df_delta.drop(columns = ['delta'], inplace = True)    
    df_delta.to_csv(os.path.join(base_path, 'res', 'ribcoz_iss_delta.csv'), sep = ';', index = False,
                    decimal = ',', encoding = 'cp1251')

df_ribcoz_iss.drop(columns = ['delta'], inplace = True)    
df_ribcoz_iss.to_csv(os.path.join(base_path, 'res', 'ribcoz_iss_sverka.csv'), sep = ';', index = False,
                     decimal = ',', encoding = 'cp1251')
print('Контроль сверки РИБ ЦОЗ выдачи: {}. Файл с расхождениями: {}.'.format(res[0], res[1]))


df_ribcoz_pay1 = pd.merge(left = df_pays[['PaymentID', 'Номер договора', 'Дата получения денежных средств на счет, руб.',
                                          'Сумма платежа', 'КБК офиса выдавшего займ', 'Организация']],
                          right = df_trans, left_on = 'PaymentID', right_on = 'tPaymentID', how = 'outer')

df_ribcoz_pay1.rename(columns = {'Номер договора': 'Договор', 'Дата получения денежных средств на счет, руб.': 'Дата из ПО',
                                 'Сумма платежа': 'Сумма из ПО', 'КБК офиса выдавшего займ': 'КБК офиса выдачи'}, inplace = True)

df_ribcoz_pay2 = pd.merge(left = df_serv[['PaymentID', 'Номер договора', 'Дата погашения',
                                          'Общая сумма платежа', 'КБК выдачи займа', 'Организация']],
                          right = df_trans, left_on = 'PaymentID', right_on = 'tPaymentID', how = 'outer')

df_ribcoz_pay2.rename(columns = {'Номер договора': 'Договор', 'Дата погашения': 'Дата из ПО',
                                 'Общая сумма платежа': 'Сумма из ПО', 'КБК выдачи займа': 'КБК офиса выдачи'}, inplace = True)
df_ribcoz_pay = pd.concat(objs = [df_ribcoz_pay1, df_ribcoz_pay2], ignore_index = True)

df_ribcoz_pay['Сумма из ПО'] = list(map(lambda x: float(str(x).replace(',', '.')), df_ribcoz_pay['Сумма из ПО']))
df_ribcoz_pay['Сумма_платежа'] = list(map(lambda x: float(str(x).replace(',', '.')), df_ribcoz_pay['Сумма_платежа']))
df_ribcoz_pay['Размер_комиссии'] = list(map(lambda x: float(str(x).replace(',', '.')), df_ribcoz_pay['Размер_комиссии']))
df_ribcoz_pay['доп_поле_2'] = list(map(lambda x: float(str(x).strip().split(' ')[0].replace(',', '.')), df_ribcoz_pay['доп_поле_2']))

df_ribcoz_pay_bind = df_ribcoz_pay[df_ribcoz_pay['Тип_операции'] == 'binding']
df_ribcoz_pay_bind['PaymentID'] = df_ribcoz_pay_bind['tPaymentID']
df_ribcoz_pay_bind.drop_duplicates(subset = ['PaymentID', 'Тип_транзакции'], inplace = True)

df_ribcoz_pay = df_ribcoz_pay[(pd.isnull(df_ribcoz_pay['PaymentID']) == False)\
                            & (pd.isnull(df_ribcoz_pay['tPaymentID']) == False)]
assert len(df_ribcoz_pay['Организация'].unique()) <= 2

df_ribcoz_pay['delta_sum'] = df_ribcoz_pay['Сумма из ПО'] + df_ribcoz_pay['Размер_комиссии'] - df_ribcoz_pay['Сумма_платежа']
df_ribcoz_pay['delta_fee'] = df_ribcoz_pay['Размер_комиссии'] - df_ribcoz_pay['доп_поле_2']

if set(df_ribcoz_pay['delta_sum']) == {0} and set(df_ribcoz_pay['delta_fee']) == {0}:
    res = ['пройден', 'не сформирован']
else:
    res = ['не пройден', 'сформирован']
    df_delta = df_ribcoz_pay[(df_ribcoz_pay['delta_sum'] != 0) | (df_ribcoz_pay['delta_fee'] != 0)]
    df_delta.drop(columns = ['delta_sum', 'delta_fee', 'tPaymentID'], inplace = True)
    df_delta.to_csv(os.path.join(base_path, 'res', 'ribcozglav_pay_delta.csv'), sep = ';', index = False,
                    decimal = ',', encoding = 'cp1251')

df_ribcoz_pay_coz = df_ribcoz_pay[df_ribcoz_pay['Организация'] == 'ООО МКК "ЦОЗ"']
df_ribcoz_pay_coz.drop(columns = ['delta_sum', 'delta_fee', 'Организация', 'tPaymentID'], inplace = True)
df_ribcoz_pay_coz = pd.concat(objs = [df_ribcoz_pay_coz, df_ribcoz_pay_bind], join = 'inner', ignore_index = True)
df_ribcoz_pay_coz.to_csv(os.path.join(base_path, 'res', 'ribcoz_pay_sverka.csv'), sep = ';', index = False,
                         decimal = ',', encoding = 'cp1251')

df_ribcoz_pay_gla = df_ribcoz_pay[df_ribcoz_pay['Организация'] == 'ООО "Главколлект"']
df_ribcoz_pay_gla.drop(columns = ['delta_sum', 'delta_fee', 'Организация', 'tPaymentID'], inplace = True)
df_ribcoz_pay_gla.to_csv(os.path.join(base_path, 'res', 'ribglav_pay_sverka.csv'), sep = ';', index = False,
                         decimal = ',', encoding = 'cp1251')
print('Контроль сверки РИБ ЦОЗ ГК платежи: {}. Файл с расхождениями: {}.'.format(res[0], res[1]))